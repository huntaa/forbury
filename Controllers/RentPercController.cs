using Microsoft.AspNetCore.Mvc;

namespace my_new_app.Controllers{

    [ApiController]
    [Route("[controller]")]
    public class RentPercController : ControllerBase {

        public RentPercController() {
            Console.Write("constructed RentPercController");
        }

        [HttpGet]
        public Result Get(
            [FromQuery]float year_one_sales,
            [FromQuery]float base_rent_1to5,
            [FromQuery]float base_rent_6to10,
            [FromQuery]float tier_1_perc,
            [FromQuery]float tier_2_perc,
            [FromQuery]float tier_3_perc,
            [FromQuery]float annual_growth_perc
        )
        {
            Result r = new Result();

            float[] sales_preds = RentPercCalculator.calc_annual_sales(year_one_sales, annual_growth_perc);

            float T1_lower_1to5 = base_rent_1to5 / 0.08f;
            float T1_upper_1to5 = base_rent_1to5 / 0.04f;
            float T2_lower_1to5 = base_rent_1to5 / 0.04f;
            float T2_upper_1to5 = base_rent_1to5 / 0.02f;
            float T3_lower_1to5 = base_rent_1to5 / 0.02f;

            float T1_lower_6to10 = base_rent_6to10 / 0.08f;
            float T1_upper_6to10 = base_rent_6to10 / 0.04f;
            float T2_lower_6to10 = base_rent_6to10 / 0.04f;
            float T2_upper_6to10 = base_rent_6to10 / 0.02f;
            float T3_lower_6to10 = base_rent_6to10 / 0.02f;

            r.PercRentTier1 = RentPercCalculator.calc_tier_perc(sales_preds, T1_lower_1to5, T1_upper_1to5, tier_1_perc, 0, ref r.PercRentTier1);
            r.PercRentTier1 = RentPercCalculator.calc_tier_perc(sales_preds, T1_lower_6to10, T1_upper_6to10, tier_1_perc, 5, ref r.PercRentTier1);

            r.PercRentTier2 = RentPercCalculator.calc_tier_perc(sales_preds, T2_lower_1to5, T2_upper_1to5, tier_2_perc, 0, ref r.PercRentTier2);
            r.PercRentTier2 = RentPercCalculator.calc_tier_perc(sales_preds, T2_lower_6to10, T2_upper_6to10, tier_2_perc, 5, ref r.PercRentTier2);

            r.PercRentTier3 = RentPercCalculator.calc_tier_perc(sales_preds, T3_lower_1to5, -1, tier_3_perc, 0, ref r.PercRentTier3);
            r.PercRentTier3 = RentPercCalculator.calc_tier_perc(sales_preds, T3_lower_6to10, -1, tier_3_perc, 5, ref r.PercRentTier3);

            r.TotalPercRent = RentPercCalculator.calc_total_perc(r.PercRentTier1, r.PercRentTier2, r.PercRentTier3);
            return r;
        }

    }

    public class RentPercCalculator {

        public static float[] calc_annual_sales(float year_one_sales, float annual_growth_perc) {
            float[] res = new float[10];
            res[0] = year_one_sales;
            
            foreach(int i in Enumerable.Range(0, 9)) {
                res[1+i] = res[i] + res[i] * annual_growth_perc;
            }

            return res;
        }

        public static float[] calc_tier_perc(float[] sales_preds, float lower_bound, float upper_bound, float rent_perc, int offset, ref float[] res) {

            foreach(int i in Enumerable.Range(0, 5)) {
                float sales_index = sales_preds[i + offset];
                res[i + offset] = 0;

                if(sales_index > lower_bound) {
                    res[i + offset] += (sales_index - lower_bound) * rent_perc;
                }

                // if upper_bound is negative then there is no upper bound
                if(upper_bound >= 0 && sales_index > upper_bound) {
                    // negate rent exceeding upper bound for this tier
                    res[i + offset] -= (sales_index - upper_bound) * rent_perc;
                }
            }

            return res;
        }

        public static float[] calc_total_perc(float[] tier1_rents, float[] tier2_rents, float[] tier3_rents) {
            float[] res = new float[10];

            foreach(int i in Enumerable.Range(0, 10)) {
                res[i] = tier1_rents[i] + tier2_rents[i] + tier3_rents[i];
            }

            return res;
        }

    }

    public class Result {
        public float[] PercRentTier1 = new float[10];
        public float[] PercRentTier2 = new float[10];
        public float[] PercRentTier3 = new float[10];
        public float[] TotalPercRent = new float[10];
    }
}