import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PercFormComponent } from './perc-form.component';

describe('PercFormComponent', () => {
  let component: PercFormComponent;
  let fixture: ComponentFixture<PercFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PercFormComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PercFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
