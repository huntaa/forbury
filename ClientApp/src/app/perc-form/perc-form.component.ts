import { Component, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { FormControl, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-perc-form',
  templateUrl: './perc-form.component.html',
  styleUrls: ['./perc-form.component.css']
})
export class PercFormComponent {
  public percRent : PercRentResult = new PercRentResult([1], [2], [3], [4]);

  public rentForm = new FormGroup ({
    yearOneSales : new FormControl(),
    baseRent1to5 : new FormControl(),
    baseRent6to10 : new FormControl(),
    tier1Perc : new FormControl(), 
    tier2Perc : new FormControl(),
    tier3Perc : new FormControl(),
    annualGrowthPerc : new FormControl()
  });

  constructor(private http: HttpClient,@Inject('BASE_URL') private baseUrl: string) {}

  getPercRent()
  {
    const params = new HttpParams()
      .set('year_one_sales', this.rentForm.value.yearOneSales)
      .set('base_rent_1to5', this.rentForm.value.baseRent1to5)
      .set('base_rent_6to10', this.rentForm.value.baseRent6to10)
      .set('tier_1_perc', this.rentForm.value.tier1Perc)
      .set('tier_2_perc', this.rentForm.value.tier2Perc)
      .set('tier_3_perc', this.rentForm.value.tier3Perc)
      .set('annual_growth_perc', this.rentForm.value.annualGrowthPerc);

    this.http.get<PercRentResult>(this.baseUrl + 'rentperc', {params}).subscribe(result => {
      //this.percRent = result;
      var res = result;
      this.percRent = result;
      console.log(res);
    })
  }
}


class PercRentResult {
  constructor(
      public PercRentTier1 : number[],
      public PercRentTier2 : number[],
      public PercRentTier3 : number[],

      public TotalPercRent : number[]
    ) {};
  
}